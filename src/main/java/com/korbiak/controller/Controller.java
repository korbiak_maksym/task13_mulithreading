package com.korbiak.controller;

import com.korbiak.model.Model;

public class Controller {

    private Model model;

    public Controller() {
        this.model = new Model();
    }

    public String getPingPong() {
        return model.getPingPong();
    }

    public String getFibonacci(int n){
        return model.getFibonacci(n);
    }

    public int getExFibonacci(int n){
        return model.getExFibonacci(n);
    }

    public String getSleepPool(int n){
        return model.getSleepPool(n);
    }

    public void testSyn(){
        model.testSynchronized();
    }

}
