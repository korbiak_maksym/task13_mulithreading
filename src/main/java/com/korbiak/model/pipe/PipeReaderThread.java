package com.korbiak.model.pipe;

import java.io.PipedReader;

public class PipeReaderThread implements Runnable {
    PipedReader pr;
    String name = null;

    public PipeReaderThread(String name, PipedReader pr) {
        this.name = name;
        this.pr = pr;
    }

    public void run() {
        try {
            // continuously read data from stream and print it in console
            while (true) {
                char c = (char) pr.read(); // read a char
                System.out.print(c);
            }
        } catch (Exception ignored) {
        }
    }
}
