package com.korbiak.model.pipe;

import java.io.PipedReader;
import java.io.PipedWriter;

public class Pipe {
    public static void main(String[] args) {
        new Pipe();
    }

    public Pipe() {
        try {
            PipedReader pr = new PipedReader();
            PipedWriter pw = new PipedWriter();
            pw.connect(pr);


            Thread thread1 = new Thread(new PipeReaderThread("ReaderThread", pr));
            Thread thread2 = new Thread(new PipeWriterThread("WriterThread", pw));

            thread1.start();
            thread2.start();

        } catch (Exception ignored) {
        }
    }
}
