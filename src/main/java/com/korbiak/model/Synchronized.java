package com.korbiak.model;

public class Synchronized {
    private static Object object = new Object();

    public void firstMethod() {
        new Thread(() -> {
            synchronized (object) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void secondMethod() {
        new Thread(() -> {
            synchronized (object) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public void thirdMethod() {
        new Thread(() -> {
            synchronized (object) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
