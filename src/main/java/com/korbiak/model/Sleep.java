package com.korbiak.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.*;

public class Sleep {

    private int n;
    private Random rand;
    volatile private String answer = "";
    private DateTimeFormatter dtf;

    Sleep(int n) {
        rand = new Random();
        this.n = n;
        dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    }

    class MyThreat implements Runnable {
        @Override
        public void run() {
            LocalDateTime now = LocalDateTime.now();
            answer += "\nExit:" + dtf.format(now);
        }
    }

    String getSleep() {
        int sumOfSec = 0;
        ScheduledExecutorService threadPoolExecutor = Executors.newScheduledThreadPool(n);
        for (int i = 0; i < n; i++) {
            int sec = rand.nextInt(10);
            Thread thread = new Thread(new MyThreat(), "Thread " + i);
            answer += thread.getName() + ": sleep time = " + sec + "s.\n";
            sumOfSec += sec;
            threadPoolExecutor.schedule(thread, sec * 1000, TimeUnit.MILLISECONDS);
        }
        threadPoolExecutor.shutdown();
        try {
            Thread.sleep(sumOfSec * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return answer;
    }
}
