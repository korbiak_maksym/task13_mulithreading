package com.korbiak.model;

import java.util.concurrent.ArrayBlockingQueue;

public class BlockingQueue {
    static ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(5);

    private static class Thread1 implements Runnable {

        @Override
        public void run() {
            for (int i = 0; i < 5; i++) {
                try {
                    queue.put(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("put:" + i);
            }
        }
    }

    private static class Thread2 implements Runnable {

        @Override
        public void run() {

            for (int i = 0; i < 5; i++) {
                try {
                    Thread.sleep(100);
                    System.out.println("Get:" + queue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(new Thread1(), "1");
        Thread thread2 = new Thread(new Thread2(), "2");
        thread1.start();
        thread2.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}


