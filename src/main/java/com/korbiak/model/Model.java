package com.korbiak.model;

import java.util.Arrays;

public class Model {
    PingPong pingPong;
    Synchronized syn;

    public Model() {
        pingPong = new PingPong();
        syn = new Synchronized();
    }

    public String getPingPong() {
        return pingPong.run();
    }

    public String getFibonacci(int n) {
        Fibonacci fibonacci = new Fibonacci(n);
        String answer;
        answer = Arrays.toString(fibonacci.run());
        return answer;
    }

    public int getExFibonacci(int n) {
        ExFibonacci exFibonacci = new ExFibonacci(n);
        return exFibonacci.getFibonacci();
    }

    public String getSleepPool(int n) {
        Sleep sleep = new Sleep(n);
        return sleep.getSleep();
    }

    public void testSynchronized() {
        syn.firstMethod();
        syn.secondMethod();
        syn.thirdMethod();
    }

}
