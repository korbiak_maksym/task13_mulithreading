package com.korbiak.model;

import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReadWriteLock {

    private static ReentrantReadWriteLock rw = new ReentrantReadWriteLock();
    private static Lock readLock = rw.readLock();
    private static Lock writeLock = rw.writeLock();

    static int[] arr = new int[7];

    public static void firstMethod() {
        new Thread(() -> {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            readLock.lock();
            System.out.print("\n First(reading): ");
            Arrays.stream(arr).forEach(el -> {
                System.out.print(el + " ");
            });
            readLock.unlock();
        }).start();
    }

    public static void secondMethod() {
        new Thread(() -> {
            writeLock.lock();
            for (int i = 0; i < 7; i++) {
                System.out.print("\n Second(writing)... ");
                arr[i] = i;
            }
            writeLock.unlock();
        }).start();

    }

    public static void thirdMethod() {
        new Thread(() -> {
            readLock.lock();
            System.out.print("\n Third(reading): ");
            Arrays.stream(arr).forEach(el -> {
                System.out.print(el + " ");
            });
            readLock.unlock();
        }).start();
    }

    public static void main(String[] args) {
        firstMethod();
        thirdMethod();
        secondMethod();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
