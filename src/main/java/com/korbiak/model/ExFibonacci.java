package com.korbiak.model;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExFibonacci {
    private int n;
    private int[] array;

    ExFibonacci(int n) {
        this.n = n;
        this.array = new int[n];
        array[0] = 0;
        array[1] = 1;
    }

    int getFibonacci() {

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        Future<Integer> answer = executorService.submit(() -> {
            int sum = 0;
            for (int i = 2; i < n; i++) {
                array[i] = array[i - 1] + array[i - 2];
                sum += array[i];
            }
            return sum + 1;
        });
        executorService.shutdown();
        try {
            return answer.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
