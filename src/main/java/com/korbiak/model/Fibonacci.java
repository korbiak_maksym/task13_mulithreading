package com.korbiak.model;

public class Fibonacci {
    private volatile int n;
    private volatile int[] array;
    private Object sync = new Object();

    public Fibonacci(int n) {
        this.n = n;
        array = new int[n];
        array[0] = 0;
        array[1] = 1;
    }

    private class Fi1 implements Runnable {
        @Override
        public void run() {
            synchronized (sync) {
                for (int i = 2; i < n; i += 2) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    array[i] = array[i - 1] + array[i - 2];
                    sync.notify();
                }
            }
        }
    }

    private class Fi2 implements Runnable {
        @Override
        public void run() {
            synchronized (sync) {
                for (int i = 3; i <= n; i += 2) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (i >= n) break;
                    array[i] = array[i - 1] + array[i - 2];
                }
            }
        }
    }

    int[] run() {
        Thread fi1 = new Thread(new Fi1(), "Fi1");
        Thread fi2 = new Thread(new Fi2(), "Fi2");
        fi1.start();
        fi2.start();
        try {
            fi1.join();
            fi2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return array;
    }

}
