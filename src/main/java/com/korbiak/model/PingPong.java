package com.korbiak.model;

class PingPong {

    private static final Object sync = new Object(); //monitor
    private volatile String answer = "";

    private class Ping implements Runnable {
        @Override
        public void run() {
            synchronized (sync) {
                for (int i = 0; i < 10; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    answer += " ping-";
                    sync.notify();
                }
            }
        }
    }

    private class Pong implements Runnable {
        @Override
        public void run() {
            synchronized (sync) {
                for (int i = 0; i < 10; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    answer += "pong ";
                }
            }
        }
    }

    String run() {
        Thread ping = new Thread(new Ping(), "Ping");
        Thread pong  = new Thread(new Pong(), "Pong");
        ping.start();
        pong.start();
        try {
            ping.join();
            pong.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return answer;
    }


}
